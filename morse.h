#include <stdbool.h>

const char * char_to_morse(char letter);
char * string_to_morse(char* string);
const char morse_to_char(const char* morse);
char * morse_to_string( const char* morse);
